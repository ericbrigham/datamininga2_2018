# Instructions for Building and Running the Code

To build and run the code follow these instructions:  

- Clone the git repository so that you have all the files on your machine.   

- Open the terminal/command line.  

- Navigate to the file "datamininga2_2018", this is where the data and the algorithms live.  

- Once here, navigate to "src." You can run the algorithms as follows:   

    - For K-Medoids type in "python kmedoids.py 5"   

    - For K-Means  : Type "Python KMeans.py K" into the command line. K is however many clusters you want. We use 5 (instructions: "Python KMeans.py 5")

    - For HClust, if you are on a Windows computer, type "java -jar hclust.jar". If you are on a Mac or Linux machine, type "java -jar hclust_linux.jar".
    

 