#A template for the implementation of K-Medoids.
import math
import sys
import csv

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    summ = 0
    for x in range (len(a)):
        diff = abs(a[x] - b[x])
        summ += diff
    mhd = summ
    return mhd 

    #summ = 0
    #for x in range (len(a)):
    #    diff = a[x] - b[x]
    #    summ += diff**2
    #euclidean_dist = math.sqrt(summ)
    #return euclidean_dist 

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
    clusters = dict()
    for x in D:
        nearest = centers[0]
        for n in centers:
            if Distance(n, x) < Distance(nearest, x):
                nearest = n
        clusters.setdefault(tuple(nearest), []).append(x)
    return clusters

# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(c, cluster):
    for cand in cluster:
        dist_c = 0
        dist_cand = 0
        for point in cluster:
            dist_c += Distance(c, point)
            dist_cand += Distance(cand, point)
        if dist_cand < dist_c:
            c = cand
    return c 

# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
    centers = D[0:k]
    old_centers = []
    clusters = dict()
    while centers != old_centers:
        old_centers= []
        for n in range (len(centers)):
            old_centers.append(centers[n])
        clusters = assignClusters(D, centers)
        for c in centers:
            centers[centers.index(c)] = findClusterMedoid(c, clusters.get(tuple(c)))
    return clusters

def main():
    D = []
    k = int(sys.argv[1])

    with open('../data/Mall_Customers_edit_csv.csv') as csvf:
        read_file = csv.reader(csvf, delimiter=',')
        c = 0
        for line in read_file:
            #print(line)
            if c != 0:
                row = []
                for feature in line:
                    row.append(float(feature))
                    #should be fine to cast anything to a float
                D.append(row)
            c += 1
    #print(D)

    #print my cluster:
    results = KMedoids(D, k)
    for r in results:
        print(r, results[r])

if __name__ == "__main__":
    main()





