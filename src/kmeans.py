#Hunter Mundy
#Data Mining Assignment 2 Final Varient  
import math
import csv
import random
import sys

def euclidianDistance(pointA, pointB):
    #Computes the euclidian distance between two points. 
    #Formula: dist((x, y), (a, b)) = √(x - a)² + (y - b)²
    differenceIncome = math.pow((pointA[0] - pointB[0]), 2)     #.pow is power
    differenceRating = math.pow((pointA[1] - pointB[1]), 2)
    combo = differenceIncome + differenceRating
    return math.sqrt(combo)                                     #Returns the squareroot

#Returns a dictionary of lists 
def assignClusters(dataPoints, centers):                        #DataPoints is a list and is all the data we have. Centers is a list and are what we say are the centers of the data
    #clusters = {cent: [] for cent in centers}                  #Makes a dictionary. Stuff inside {} establishes the keys will have lists for values. There are as many lists as there are centers
    #clusters = defaultdict(list{cent: [] for cent in centers})
    #clusters = {cent: () for cent in centers}
    #clusters = {}
    #Eventually followed the assignClusters function in Eric's KMedoids function as all the ways I tried to do it wouldn't work.
    clusters = dict()                                           #Makes a dictionary.
    for points in dataPoints:                                   #For everything in the data
        closestPoint = random.choice(centers)                   #Chooses a random value for inital center
        #print("Random Centers are")
        #print(centers)
        for middles in centers:
##            print("The Middles is")                           #Used for testing. 
##            print(middles)
##            print("Points is")
##            print(points)
##            print("Closest point is ")
##            print(closestPoint)
            #Current problem is that middles and closetpoint are somehow being chosen as a single data point
            #After printing it seems that the entire Kmeans loops once. It looks like the results of the cluster mean function are
            #being put into assignclusters function on the second loop.
            #UPDATE: Current problem is the "for middles in centers:" part. It works find for the first loop through KMeans but on the second
            #loop, centers is only a single list with two floats inside it.
            #UPDATE: FIXED. See Solved below in KMeans. 
            if euclidianDistance(middles, points) < euclidianDistance(closestPoint, points):     #If a new point is closer
                #print("Inside eculid IF")
                closestPoint = middles                          #Make it the new middle.
        #clustList = list(clusters)                             #Attempt to fix problem by converting tuple to list and back. Didn't work
        #clustList[closestPoint].append(points) 
        clusters.setdefault(tuple(closestPoint), []).append(points) #This way works. Above ones don't. 
        #print(clusters)
        #clusters = clustList
    #print(clusters)
    return clusters

def findClusterMean(cluster):                                  #Just returns the mean of the group of points
    sumFirst = 0.0
    sumSecond = 0.0
    for things in cluster:
        sumFirst += things[0]
        sumSecond += things[1]
    newX = (sumFirst/len(cluster))                             #Divides by the number of things in the cluster
    newY = (sumSecond/len(cluster))
    newMiddle = [newX, newY]
    #print("Our new Middle is")
    #print(newMiddle)
    return newMiddle


def KMeans(dataGroup, K):                                   #Needs to return a set of lists. 
    foundCenters = random.sample(dataGroup, K)              #Randomly creates initial centers
    #print("The first found centers are")                   #All the commented out code was used in tests. 
    #print(foundCenters)
    oldCenters = []                                         #Value we'll compare against in the while loop. Is intially nothing
    theClusters = set()                                     #Set of lists
    while foundCenters != oldCenters:                       #We want to stop when there's no more change detected.
##        print("LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOPING")
##        print("foundcenters before are")
##        print(foundCenters)
##        print("oldCenters before are")
##        print(oldCenters)
        oldCenters = foundCenters                           #Overide the old middles with new ones.
##        print("foundcenters after are")
##        print(foundCenters)
##        print("oldCenters after are")
##        print(oldCenters)
        #UPDATE: I think the problem might be because I'm not dividing the clusters. They'er being put in one large cluster. UPDATE:
        #I am putting them in multiple clusters. Something isn't working. Possibly in the ClusterMeans.
        #SOLVED: Forgot to loop through each cluster in theClusters for the findClusterMean part. Because I
        #didn't, it only selected a new mean for one cluster, which gave a single list of two floats. 
        theClusters = assignClusters(dataGroup, oldCenters) #Should work. Since we only want the clusters it found at the end, we can keep overriding them
        for regions in theClusters:
            foundCenters.append(findClusterMean(theClusters))         #Updates the new middle of a cluster
    return theClusters

def csvOpener():                                                #Opens the CSV and reads the rows. Converted from Eric's KMedoids main function.
    ListofClusters =[]   
    #print("Got in")
    #with open('..\Data Mining 2018\Mall_Customers_edit_csv.csv') as csvf:  #For my computer
    with open('../data/Mall_Customers_edit_csv.csv') as csvf: #For all computers
        read_file = csv.reader(csvf, delimiter=',')             #reader reads each row in a csv file. delimiter tells the reader, 
        skipRow = 0                                             #We need to skip the the first row as that just has the column names
        for line in read_file:                                  #
            #print(line)
            if skipRow != 0:                                    #This skips the first row. 
                row = []
                for feature in line:
                    row.append(float(feature))                  #Changes values to floats in preparation for division later
                ListofClusters.append(row)
            skipRow += 1
    #print(ListofClusters)
    #print("Done Reading")
    return ListofClusters


#Decided to leave in my tests for record keeping.

def distanceTester():
    #Eudlicdean will take X and Y for two points.
    #Returns double
    pointAlpha = [2, 2]
    pointBeta = [1, 1]
    testOne = euclidianDistance(pointAlpha, pointBeta)
    print(testOne)
    print(math.sqrt(2))     #Should be the same as testOne
    return testOne

def clusteringTest():
    point1 = (2, 2)         #Makes points. This are tuples. Tuples are needed to stop angering Python. It doesn't like lists here
    point2 = (1, 1)
    point3 = (44, 46)
    point4 = (45, 47)
    point5 = (10, 13)
    point6 = (50, 50)
    point7 = (72, 80)
    point8 = (82, 90)
    EXPData = []
    EXPData.append(point1)  #Appends points to list
    EXPData.append(point2)
    EXPData.append(point3)
    EXPData.append(point4)
##    EXPData.append(point5)  #Use points 1-4 for a K of two. Add more to test other ks and correct clustering
##    EXPData.append(point6)
##    EXPData.append(point7)
##    EXPData.append(point8)
    TestCenters = []        #Creates some simple centers. Centers are assigned from known points at first. 
    TestCenters.append(point2) 
    TestCenters.append(point3)
    resulter = assignClusters(EXPData, TestCenters)
    print(resulter)         
    return resulter

def meantester():
    point1A = (2, 2)
    point2B = (4, 3)
    point3C = (10, 13)
    TestData = []
    TestData.append(point1A)  #Appends points to list
    TestData.append(point2B)
    TestData.append(point3C)
    meanValue = findClusterMean(TestData)
    print(meanValue[0])     #Should be the same as the line below it. 
    print(16/3)
    print("Second Ones")
    print(meanValue[1])
    print(18/3)
    return meanValue

            #Test Segment#
#print("We are here")
#testing = dictionaryMaker()
#print(testing)
#readerExperiment = csvOpener()      #It works
#print(readerExperiment)
#distExperiment = distanceTester()
#print("Inputted")
#print(distExperiment)
#CLB = clusteringTest()
#print(CLB)
#findMiddles = meantester()
#print(findMiddles)
#19007  21753  24491   27648  30800. These were the line numbers of the last tests I ran during bug fixing. 


            #Main Part#
#NOTE: Following works only when it is ran through a command line.
#Command Line Instructions: Go to where KMeans is (in the "src" file) via the command prompt
#Type "Python KMeans.py K" into the command line. K is however many clusters you want. Press enter. 
def main():                                     #Converted from Eric's main function in KMedoids
    D = []
    #k = 3                                      #Used for tests. Put in what k you want. #Tests:Works when k is 1,2,3,4,5,10,100, and 200.
                                                #Should work for any K provided it isn't bigger than data set.
    #**WARNING**: There is only 201 pairs in the data. Max K is 200! Will BREAK if more than 200 is used!! **WARNING**
    k = int(sys.argv[1])                        #Grabs the user inputted K value from the Command line. 
    listofKClusters = csvOpener()               #Reads the file
    results = KMeans(listofKClusters, k)        #Runs KMeans
    for r in results:                           #Prints the results for viewing.
        print(r, results[r])

if __name__ == "__main__":
    main()
