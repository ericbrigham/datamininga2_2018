import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVReader {
    
    public CSVReader() {
    }
    
    public ArrayList<point> readCSV(String csvFile) {
        
        BufferedReader br = null;
        String line;
        String csvSplitBy = ",";
        
        ArrayList<point> data = new ArrayList();
        point currentPoint;
        
        try {
            FileReader fr = new FileReader(csvFile);
            br = new BufferedReader(fr);
            
            while ((line = br.readLine()) != null) {
                int x = Integer.parseInt(line.split(csvSplitBy)[0]);
                int y = Integer.parseInt(line.split(csvSplitBy)[1]);
                
                currentPoint = new point(x, y);
                data.add(currentPoint);
            }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return data;
    }       
}