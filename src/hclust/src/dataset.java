import java.util.ArrayList;

public class dataset {
    
    String csvFile;
    CSVReader reader;
    ArrayList<point> data;
    
    public dataset(String csv) {
        reader = new CSVReader();
        this.csvFile = csv;
        data = reader.readCSV(csvFile);
    }
    
    public void printData() {
        for (point p : data) {
            p.printPoint();
        }
    }
    
    // Finds the distance between two points a and b.
    public double getDistance(point a, point b) {
        int dx, dy;
        double distance;
        
        dx = Math.abs(a.getX() - b.getX());
        dy = Math.abs(a.getY() - b.getY());
        
        distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
        
        return distance;
    }
    
    // Produces a point that is the average of two points a and b.
    public point merge(point a, point b) {
        int px, py;
        point p;
        
        px = ((a.getX() + b.getX()) / 2);
        py = ((a.getY() + b.getY()) / 2);
        
        p = new point(px, py);
        
        return p;
    }
    
    // Finds the closest point in the dataset to a given point
    public point findClosest(point a) {
        point closestPoint = a;
        double currentDistance;
        double shortestDistance = 1000;
        
        for (point p : data) {
            currentDistance = getDistance(a, p);
            if (currentDistance < shortestDistance && a != p) {
                shortestDistance = currentDistance;
                closestPoint = p;
            }
        }
        return closestPoint;
    }
    
    // Finds the pair of points in the dataset that are the closest
    public point[] findClosestPair() {
        point[] pair = new point[2];
        point closestPoint;
        double currentDistance;
        double shortestDistance = 1000;
        
        for (point p : data) {
            closestPoint = findClosest(p);
            currentDistance = getDistance(p, closestPoint);
            if (currentDistance < shortestDistance && p != closestPoint) {
                shortestDistance = currentDistance;
                pair[0] = p;
                pair[1] = closestPoint;
            }
        }
        
        return pair;
    }
    
    // First, finds the two closest points in the dataset.
    // Next, produces a point that is the average of those two points.
    // Then adds the newly-created point to the dataset and removes the original points.
    // Finally, prints all points in the current version of the dataset.
    // Repeats this process until there is only one point left in the dataset.
    public void HClust() {
        
        point[] closestPair;
        point newPoint;
        int numPoints = data.size();
        
        while (numPoints > 1) {
            closestPair = findClosestPair();
            newPoint = merge(closestPair[0], closestPair[1]);
            data.remove(closestPair[0]);
            data.remove(closestPair[1]);
            data.add(newPoint);
            numPoints = data.size();
            
            System.out.println("New iteration!");
            printData();
        }
        
        System.out.println("Done!");
    }
}
