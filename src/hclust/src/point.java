public class point {
    
    int x, y;
    
    public point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    // Prints the point's coordinates.
    public void printPoint() {
        System.out.println(x + ", " + y);
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
}
